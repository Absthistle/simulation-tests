package sim;

public abstract class SimObject {
    public int x;
    public int y;
    public int width;
    public int height;

    public SimObject(Simulation simulation, int x, int y, int width, int height) {
        this.x=x-width;
        this.y=y-height;
        this.width=width;
        this.height=height;

        this.setSim(simulation);
    }

    protected abstract void setSim(Simulation simulation); //You'll likely include the test object for reference. Just a reminder.
}
