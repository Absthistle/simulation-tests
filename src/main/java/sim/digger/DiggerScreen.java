package sim.digger;

import sim.BaseSimScreen;
import sim.Simulation;

import java.awt.*;
import java.util.Arrays;

public class DiggerScreen extends BaseSimScreen {
    protected DiggerSim sim;
    public DiggerScreen() {
        setBackground(Color.BLACK);
    }
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);


        if(sim==null) {return;} //Don't begin drawing until simulation has initiated.

        //Draw every colour from the map of markings.
        for(int i = 0; i<sim.marked.length; i++) {
            if(sim.marked[i] != 0) {
                g.setColor(new Color(sim.marked[i]));
                g.fillRect((i%sim.length)*DiggerSim.DIGGER_SIZE, (i/sim.length)*DiggerSim.DIGGER_SIZE, DiggerSim.DIGGER_SIZE, DiggerSim.DIGGER_SIZE);
            }
        }

        //Draw diggers as white.
        g.setColor(Color.WHITE);
        Digger[] diggers = sim.diggers.toArray(Digger[]::new); //Cannot use raw list, ere we summon a concurrent modification exception.
       Arrays.stream(diggers).forEach(digger -> g.fillRect(digger.x, digger.y, digger.width, digger.height));
    }

    @Override
    public void setSim(Simulation simulation) {
        this.sim = (DiggerSim) simulation;
    }

    @Override public Dimension getPreferredSize() {return new Dimension(DiggerSim.DIG_AREA, DiggerSim.DIG_AREA);}
}
