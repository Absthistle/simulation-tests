package sim.digger;

import sim.SimObject;
import sim.Simulation;

import java.awt.*;

public class Digger extends SimObject {
    protected DiggerSim sim;
    public final Color markColour;

    public int[] forwardDirection = new int[]{1,0}; //X and Y, negative or positive one.
    public Digger(DiggerSim sim, int x, int y, Color markColour) {
        super(sim, x,y, DiggerSim.DIGGER_SIZE, DiggerSim.DIGGER_SIZE);
        this.markColour=markColour;
    }

    public void rotate() {
        forwardDirection[0]--;
        forwardDirection[1]++;
        if (forwardDirection[0]<-1) {
            forwardDirection[0]=1;
        }
        if(forwardDirection[1]>1) {
            forwardDirection[1]=-1;
        }
    }

    public void rotateReverse() {
        forwardDirection[0]++;
        forwardDirection[1]--;
        if (forwardDirection[1]<-1) {
            forwardDirection[1]=1;
        }
        if(forwardDirection[0]>1) {
            forwardDirection[0]=-1;
        }
    }

    public void forward(int count) {
        for(int i = 0; i < count; i++) {
            int pos = sim.getMappedPos(x, y);
            sim.marked[pos] = markColour.getRGB();
            x += forwardDirection[0] * DiggerSim.DIGGER_SIZE;
            y += forwardDirection[1] * DiggerSim.DIGGER_SIZE;

            if(!sim.isPositionValid(pos)) {
                if(DiggerSim.SCREEN_WRAP) {
                    //Set to wrap and unmap positions.
                    int[] wrapPos = sim.getWrap(x, y);
                    x = wrapPos[0];
                    y = wrapPos[1];
                } else {
                    if (DiggerSim.RANDOM_SPAWNPOINTS) {
                        //Get random position with a colour, and unmap.
                        int randPos = sim.randomMarkedPosition();
                        x = (randPos%sim.length) * DiggerSim.DIGGER_SIZE;
                        y = (randPos/sim.length) * DiggerSim.DIGGER_SIZE;
                    } else {
                        this.x = sim.graphicPane.getPreferredSize().width / 2;
                        this.y = sim.graphicPane.getPreferredSize().height / 2;
                    }
                }
            }
        }
    }

    @Override
    protected void setSim(Simulation simulation) {
        this.sim = (DiggerSim) simulation;
    }
}
