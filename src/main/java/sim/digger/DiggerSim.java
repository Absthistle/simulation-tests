package sim.digger;

import main.Main;
import sim.Simulation;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class DiggerSim extends Simulation {
    //As a result of origin bias, digger size and dig area affect how far diggers will expand.
    public final static int DIGGER_SIZE = 1; //Lower values are finer.
    public final static int DIG_AREA = 5000; //Total area of the simulation.

    //If true, diggers will rotate regardless of a mark. This overrides multiple other settings.
    public final static boolean NO_MARKS = false;

    //If true, all conditions which involve marking will produce no rotation.
    // This is the inverse of (and overrides) NO_MARKS, in which it will never rotate when mark conditions are met, as opposed to always.
    public final static boolean MARK_ROTATION = true;

    //How often the digger rotates.
    public final static float ROTATION_PERCENT = 0.5f;

    //The iteration threshold until another digger appears. Minimum 0.01f for sanity.
    public final static float DIGGER_APPEARANCE_THRESHOLD = 0.10f;

    //Multiplier of maximum iterations until the first new digger appears. 0f will remove delay.
    public final static float DIGGER_APPEARANCE_DELAY = 0.0f;

    //Whether new diggers will spawn at a random point (inside another colour).
    public final static boolean RANDOM_SPAWNPOINTS = true;

    //Maximum of diggers for each diggers per digger.
    public final static int DIGGER_MAX = 5;

    //How many diggers should spawn at once. These diggers will be of the same colour.
    public final static int DIGGERS_PER_DIGGER = 3;

    //If enabled, diggers will wrap across the screen, rather than be sent to the origin.
    public final static boolean SCREEN_WRAP = true;

    //If true, diggers only acknowledge their own colour as a mark.
    public final static boolean INDIVIDUAL_MARKING = false;


    public final List<Digger> diggers = new ArrayList<>();
    public final int length = DIG_AREA/DIGGER_SIZE; //Represents the length of a row/column when iterating over marked.
    public int[] marked = new int[(length)*(length)];
    public int newDiggerIteration = 0; //Iteration when a new digger will appear. Modify delay rather than this.
    public DiggerSim() {
        super(new DiggerScreen());

    }
    @SuppressWarnings({"ConstantConditions", "divzero", "NumericOverflow", "RedundantSuppression"})
    public void iterate(int currentIteration) {
        if(currentIteration==newDiggerIteration && diggers.size()<DIGGER_MAX*DIGGERS_PER_DIGGER) {
            //Add a new digger if the time has come.
            Color diggerColour = Color.getHSBColor((float)Math.random(),(float)Math.random()*0.7f, (float) (1-(Math.random()*0.5f)));
            int[] pos = new int[2];
            if(RANDOM_SPAWNPOINTS) {
                int randPos = randomMarkedPosition();
                //Unmap position.
                pos[0]=(randPos%length) * DiggerSim.DIGGER_SIZE;
                pos[1]=(randPos/length) * DiggerSim.DIGGER_SIZE;
            } else {
                pos[0]=graphicPane.getPreferredSize().width/2;
                pos[1]=graphicPane.getPreferredSize().height/2;
            }
            for(int i = 0; i<DIGGERS_PER_DIGGER;i++) {
                diggers.add(new Digger(this, pos[0], pos[1], diggerColour));
            }

            //Set when the next new digger will appear.
            if(newDiggerIteration==0) {newDiggerIteration = DIGGER_APPEARANCE_DELAY == 0f ? 1 : (int) (Main.ITERATIONS * DIGGER_APPEARANCE_DELAY);}
            else {newDiggerIteration += Math.max(DIGGER_APPEARANCE_THRESHOLD, 0.01f)*Main.ITERATIONS;}
        }

        for(Digger digger:diggers) {
            //Set variables.
            int diggerPos = getMappedPos(digger.x, digger.y); //Map the digger's current position to the marking grid.

            //If position is outside of bounds, reset to origin, or a random non-null colour if enabled.
            //This is ignored if wrap is enabled.
            if(!isPositionValid(diggerPos)) {
                if(DiggerSim.SCREEN_WRAP) {
                    //Set to wrap.
                    int[] wrapPos = getWrap(digger.x, digger.y);
                    digger.x = wrapPos[0];
                    digger.y = wrapPos[1];
                    diggerPos = getMappedPos(wrapPos[0], wrapPos[1]);
                } else {
                    if (RANDOM_SPAWNPOINTS) {
                        int pos = randomMarkedPosition();
                        digger.x = (pos%length) * DiggerSim.DIGGER_SIZE;
                        digger.y = (pos/length) * DiggerSim.DIGGER_SIZE;
                        diggerPos = pos;
                    } else {
                        digger.x = graphicPane.getPreferredSize().width / 2;
                        digger.y = graphicPane.getPreferredSize().height / 2;
                        diggerPos = getMappedPos(digger.x, digger.y);
                    }
                }
            }

            //Rotate if the current tile isn't marked.
            if(NO_MARKS || ((INDIVIDUAL_MARKING && (marked[diggerPos]!=digger.markColour.getRGB())) || (marked[diggerPos] == 0))) {
                if(MARK_ROTATION) {digger.rotate();}
            }
            digger.forward(1); //Will move forward and mark position.

            //Rotate according to random.
            if (Math.random() < ROTATION_PERCENT) {digger.rotate();}
        }
    }

    public void shutdown() {}
    @SuppressWarnings({"BooleanMethodIsAlwaysInverted"})
    public boolean isPositionValid(int pos) {
        return (pos > -1 && pos<marked.length);
    }

    public int getMappedPos(int x, int y) {
        return (x/DIGGER_SIZE)+((y/DIGGER_SIZE)*length);
    }

    public int randomMarkedPosition() {
        int pos = 0;

        boolean colourAtPos = false;
        int attempts = 0;
        while(!colourAtPos && attempts < 200) {
            pos = (int) (Math.random()*(marked.length));
            if(marked[pos]!=0) {
                colourAtPos=true;
            } else {
                attempts++;
            }
        }
        if(attempts==200) {
            pos = marked.length/2+length/2; //Get half Y and half X, respectively.
        }
        return pos;
    }
    public int[] getWrap(int x, int y) { //Sends wrap if the coordinates exit the mapping's bounds. Also assumes input is not mapped.
        int[] maxBounds = new int[]{graphicPane.getPreferredSize().width-DIGGER_SIZE, graphicPane.getPreferredSize().height-DIGGER_SIZE};
        if(x<0) {
            x = maxBounds[0];
        } else if (x>maxBounds[0]) {
            x = 0;
        }
        if(y<0) {
            y = maxBounds[1];
        } else if (y>maxBounds[1]) {
            y = 0;
        }
        return new int[]{x,y};
    }
}
