package sim;

import main.BasePane;

import javax.swing.*;
import java.awt.*;

public abstract class BaseSimScreen extends JPanel {
    @Override public Dimension getPreferredSize() {return new Dimension(BasePane.BASE_WIDTH,BasePane.BASE_HEIGHT);}
    public abstract void setSim(Simulation simulation);
}
