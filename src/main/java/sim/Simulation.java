package sim;

public abstract class Simulation {
    public final BaseSimScreen graphicPane;

    protected Simulation(BaseSimScreen graphicPane) {
        this.graphicPane = graphicPane;
        this.graphicPane.setLayout(null);

        graphicPane.setSim(this);
    }

    public abstract void iterate(int currentIteration); //Update simulation objects.
    public abstract void shutdown(); //Called after the final iteration.
}
