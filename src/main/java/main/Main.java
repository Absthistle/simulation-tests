package main;

import sim.Simulation;
import sim.digger.DiggerSim;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Main {
    public final static Simulation CURRENT_SIMULATION = new DiggerSim();
    public final static int ITERATIONS = 10000000;
    public final static float RENDER_THRESHOLD = 0.1f; //0.0f is every iteration. 1.0f is never.
    public final static int SLEEP_TIME = 0; //Slow the simulation by increasing this value. (Frames will not be accurate.)

    private static boolean concluded;
    public static void main(String[] args) throws InterruptedException, IOException {
        System.out.println("Initializing directories...");
        DirConfig.initializeDirectories();

        System.out.println("Initiating window...");
        javax.swing.SwingUtilities.invokeLater(() -> { //Initialize window.
            //Init window.
            JFrame frame = new JFrame("Simulation");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            //Initialize parent content pane, which contains all other content panes.
            frame.setContentPane(new BasePane());
            frame.setResizable(false);
            //Display the window.
            frame.pack();
            frame.setVisible(true);
        });

        System.out.println("Initiating simulation...");
        //Simulation loop.
        int i = 0;
        int nextRenderIteration = 0;
        while(i < ITERATIONS) {
            CURRENT_SIMULATION.iterate(i);
            if(i>=nextRenderIteration-1) {
                CURRENT_SIMULATION.graphicPane.repaint();
                nextRenderIteration+=RENDER_THRESHOLD*Main.ITERATIONS;
            }
            i++;
            Thread.sleep(SLEEP_TIME);
        }
        CURRENT_SIMULATION.graphicPane.repaint();
        CURRENT_SIMULATION.shutdown();
        concluded = true;

        System.out.println("Simulation concluded!\n");

        System.out.println("Gathering data as png...");
        Container contents = CURRENT_SIMULATION.graphicPane;
        BufferedImage image = new BufferedImage(contents.getWidth(), contents.getHeight(), BufferedImage.TYPE_INT_ARGB);
        contents.paint(image.getGraphics());


        ImageIO.write(image, "PNG", new File(DirConfig.runDir + "/latest.png")); //Pre-rerender

        System.out.println("Applying post-render...");
        image = PostRender.render(image);
        ImageIO.write(image, "PNG", new File(DirConfig.runDir + "/latest-render.png")); //Post-rerender

        System.out.println("Rendering complete!");
    }

    public static boolean isConcluded() {return concluded;}
}
