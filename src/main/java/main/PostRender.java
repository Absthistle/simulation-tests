package main;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.util.Arrays;

public class PostRender {
    public static final boolean NOISE_BLEND = false; //I'm sure there's another name for this, maybe.
    public static final int GAUSSIAN_BLEND = 1; //Value of 1 is 2x2.
    public static BufferedImage render(BufferedImage im) {
        if(NOISE_BLEND) {
            for (int y = 0; y < im.getHeight(); y++) {
                for (int x = 0; x < im.getWidth(); x++) {
                    int clr = im.getRGB(x,y)/9;
                    int[] surrounding = new int[]{
                            x-1>-1 ? im.getRGB(x-1,y)/9:clr,
                            x+1<im.getWidth()? im.getRGB(x+1,y)/9:clr,
                            y-1>-1 ? im.getRGB(x,y-1)/9:clr,
                            y+1<im.getHeight() ? im.getRGB(x,y+1)/9:clr,
                            x-1>-1 && y-1>-1 ? im.getRGB(x-1,y-1)/9:clr,
                            x+1<im.getWidth() && y-1>-1 ? im.getRGB(x+1,y-1)/9:clr,
                            x-1>-1 && y+1<im.getHeight() ? im.getRGB(x-1,y+1)/9:clr,
                            x+1<im.getWidth() && y+1<im.getHeight() ? im.getRGB(x+1,y+1)/9:clr,
                    };
                    int avg =
                            surrounding[0] + surrounding[1] + surrounding[2] +
                            surrounding[3] + clr + surrounding[4] +
                            surrounding[5] + surrounding[6] +surrounding[7];
                    im.setRGB(x, y, avg);
                }
            }
        }
        if(GAUSSIAN_BLEND>0) {
            float[] matrix = new float[(GAUSSIAN_BLEND+1)*(GAUSSIAN_BLEND+1)];
            Arrays.fill(matrix, (float) (1.0/matrix.length));
            BufferedImageOp op = new ConvolveOp(new Kernel((GAUSSIAN_BLEND+1), (GAUSSIAN_BLEND+1), matrix));
            Container contents = Main.CURRENT_SIMULATION.graphicPane;
            im = op.filter(im, new BufferedImage(contents.getWidth(), contents.getHeight(), BufferedImage.TYPE_INT_ARGB));
        }
        return im;
    }
}
