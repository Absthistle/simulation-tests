package main;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DirConfig {
    public static final File runDir = new File("run");

    public static void initializeDirectories() throws IOException {
        if(!(runDir.exists() && runDir.isDirectory())) {
            Files.createDirectory(Paths.get(String.valueOf(runDir)));
        }
    }
}
