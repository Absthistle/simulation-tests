package main;

import javax.swing.*;
import java.awt.*;

public class BasePane extends JPanel { //This is the pane which other panes are placed on.
    public static final int BASE_WIDTH = 1000;
    public static final int BASE_HEIGHT = 600;

    private final JScrollPane scroller;
    public BasePane() {
        super();

        scroller = new JScrollPane(Main.CURRENT_SIMULATION.graphicPane);
        scroller.setPreferredSize(getPreferredSize());

        scroller.getHorizontalScrollBar().setMaximum(Main.CURRENT_SIMULATION.graphicPane.getPreferredSize().width);
        scroller.getHorizontalScrollBar().setValue(Main.CURRENT_SIMULATION.graphicPane.getPreferredSize().width/2 - getPreferredSize().width/2);
        scroller.getVerticalScrollBar().setValue(Main.CURRENT_SIMULATION.graphicPane.getPreferredSize().height/2 - getPreferredSize().height/2);
        scroller.getHorizontalScrollBar().setBlockIncrement(Main.CURRENT_SIMULATION.graphicPane.getPreferredSize().width/(getPreferredSize().width/2));
        scroller.getVerticalScrollBar().setBlockIncrement(Main.CURRENT_SIMULATION.graphicPane.getPreferredSize().height/(getPreferredSize().height/2));
        scroller.getHorizontalScrollBar().setUnitIncrement(Main.CURRENT_SIMULATION.graphicPane.getPreferredSize().width/100);
        scroller.getVerticalScrollBar().setUnitIncrement(Main.CURRENT_SIMULATION.graphicPane.getPreferredSize().height/100);

        scroller.getViewport().addChangeListener(e -> {
            if(!Main.isConcluded()) {
                repaint(0,0, getSize().width, getSize().height);}
        });

        add(scroller);

        setOpaque(true);
    }

    public JScrollPane getScroller() {return scroller;}

    @Override public Dimension getPreferredSize() {
        return new Dimension(BASE_WIDTH, BASE_HEIGHT);
    }
}
