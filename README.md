# Simulation Tests

The following code includes a minimalist (and likely poorly-written) framework and sample simulations. You may implement your own code, or set a sample simulation to be run in the `Main` class. 

Core variables may be modified from the `Main` class, and those of specific tests through their respective test class.

Post-rendering can be applied by setting the variables within PostRender, though the unprocessed image will still be posted as latest.png.